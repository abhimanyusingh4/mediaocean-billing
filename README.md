# Mediaocean-Billing
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

This is a simple product, order and customer management applicaion With the follwing features.

  - Add Products
  - Add Customers
  - Create orders, Add products
  - Generate Invoices

Technology stack

  - Spring, Java 8, Junit, Spock(For integration tests and unit tests), Groovy
  - Uses H2 Database to dev purposes to tests relation and schema
  - To run integrations and demo

To build the project:
  - Simple do a maven clean install -U
  
To deploy:
- Simply run the BillingApplicaiton.class
 - The app deploys on port 8080 on local with context path as "billing/api/v1"

### Available Endpoints

* /customer: GET : customer operations
* /order : GET/PUT/POST : order operations
* /product : GET/POST : product operations
* /order/{orderId}/invoice : to generate itemized invoice
