package com.mediaocean.billing.service.unit

import com.mediaocean.billing.application.exception.EntityValidationFailedException
import com.mediaocean.billing.application.exception.ProductNotFoundException
import com.mediaocean.billing.application.model.GstSlab
import com.mediaocean.billing.application.model.Product
import com.mediaocean.billing.application.repository.ProductRepo;
import com.mediaocean.billing.application.service.ProductService;
import spock.lang.Specification;

class ProductServiceSpec extends Specification {
    ProductService productService

    ProductRepo mockProductRepo = Mock()

    def setup() {
        productService = new ProductService(mockProductRepo)
    }

    def "addProduct: Should add only valid products"() {
        given:
        List<Product> products = [
                new Product(name: "Test Product 2", price: 10.0, slab: GstSlab.SLAB_C, brandName: "Test Brand 3")
        ]

        when:
        def productResponse = productService.addProducts(products)
        then:
        1 * mockProductRepo.saveAll(products) >> products
        assert productResponse.size() == 1
    }

    def "addProduct: Should throw exception on adding invalid products"() {
        given:
        List<Product> products = [
                new Product(name: "Test Product 2", price: 0.0, slab: GstSlab.SLAB_C, brandName: "Test Brand 3")
        ]

        when:
        def productResponse = productService.addProducts(products)
        then:
        thrown EntityValidationFailedException
    }

    def "getProductById: Valid "() {
        given:
        Product product1 = new Product(name: "Test Product 2", price: 10.0, slab: GstSlab.SLAB_C, brandName: "Test Brand 3")

        when:
        Product product = productService.getProductById(1)
        then:
        1 * mockProductRepo.findById(1) >> Optional.of(product1)
        assert product1.name == product.name
        assert product1.brandName == product.brandName
        assert product1.price == product.price
    }

    def "getProductById: Should throw exception for invalid product id "() {
        when:
        productService.getProductById(1)
        then:
        1 * mockProductRepo.findById(1) >> Optional.ofNullable(null)
        thrown ProductNotFoundException
    }
}
