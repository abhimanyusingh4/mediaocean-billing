package com.mediaocean.billing.service.integration

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.mediaocean.billing.application.model.Customer
import com.mediaocean.billing.application.model.GstSlab
import com.mediaocean.billing.application.model.Invoice
import com.mediaocean.billing.application.model.Order
import com.mediaocean.billing.application.model.Product
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap

class MediaoceanBillingApplication_IT extends IT_Base {

    @Autowired
    ObjectMapper mapper

    @Test
    void "Add Products: Add products to the list of available products in the DB"() {
        when:

        List<Product> productList = [
                new Product(name: "Test Product 3", price: 456.0, slab: GstSlab.SLAB_C, brandName: "Test Brand 3"),
                new Product(name: "Test Product 4", price: 678.0, slab: GstSlab.SLAB_D, brandName: "Test Brand 4")
        ]
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>()
        headers.add("Content-Type", "application/json")
        HttpEntity<List<Product>> httpEntity = new HttpEntity<>(productList, headers)
        def productResponse = testRestTemplate.exchange("/product", HttpMethod.POST, httpEntity, String.class, headers)
        List<Product> products = mapper.readValue(productResponse.body, new TypeReference<List<Product>>() {})
        products.each { product ->
            assert product.productId != null
        }
        then:
        assert productResponse.statusCode.is2xxSuccessful()
        assert products.size() == 2
    }

    @Test
    void "Add Customer: Create customer"() {
        when:
        Customer customer = new Customer(name: "Test User 2", email: "testEmail2@gmail.com")
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>()
        headers.add("Content-Type", "application/json")
        HttpEntity<Customer> httpEntity = new HttpEntity<>(customer, headers)
        def responseEntity = testRestTemplate.exchange("/customer", HttpMethod.PUT, httpEntity, Customer.class, headers)
        Customer customerResponse = responseEntity.body
        then:
        assert responseEntity.statusCode.is2xxSuccessful()
        assert customerResponse.email == customer.email
        assert customerResponse.customerId != null
    }

    @Test
    void "Get Customer By Email"() {
        when:
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>()
        headers.add("Content-Type", "application/json")
        HttpEntity httpEntity = new HttpEntity<>(headers)
        def responseEntity = testRestTemplate.exchange("/customer/" +"testEmail1@gmail.com",  HttpMethod.GET, httpEntity, Customer.class)
        Customer customer = responseEntity.body
        then:
        assert responseEntity.statusCode.is2xxSuccessful()
        assert customer.email != null
        assert customer.customerId != null
    }

    @Test
    void "Create Order: Create an order for customer"() {
        when:
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>()
        headers.add("Content-Type", "application/json")
        HttpEntity httpEntity = new HttpEntity(headers)
        def responseEntity = testRestTemplate.exchange("/order/3/new", HttpMethod.POST, httpEntity, Order.class, headers)
        Order order = responseEntity.body
        then:
        assert responseEntity.statusCode.is2xxSuccessful()
        assert order.orderId != null
    }

    @Test
    void "Get Order: Get an order by OrderId"() {
        when:
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>()
        headers.add("Content-Type", "application/json")
        HttpEntity httpEntity = new HttpEntity(headers)
        def responseEntity = testRestTemplate.exchange("/order/4", HttpMethod.GET, httpEntity, Order.class, headers)
        Order order = responseEntity.body
        then:
        assert responseEntity.statusCode.is2xxSuccessful()
        assert order.orderId != null
    }

    @Test
    void "Add product to Order: Add product to an order by product Id"(){
        when:
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>()
        headers.add("Content-Type", "application/json")
        HttpEntity httpEntity = new HttpEntity(headers)
        def responseEntity = testRestTemplate.exchange("/order/4/product/1", HttpMethod.PUT, httpEntity, Order.class, headers)
        Order order = responseEntity.body
        then:
        assert responseEntity.statusCode.is2xxSuccessful()
        assert order.orderId != null
        assert order.products.size()!=0
    }

    @Test
    void "Generate Invoice for order: Generate Invoice for Order"(){
        when:
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>()
        headers.add("Content-Type", "application/json")
        HttpEntity httpEntity = new HttpEntity(headers)
        def order = testRestTemplate.exchange("/order/4", HttpMethod.GET, httpEntity, Order.class, headers).body
        def responseEntity = testRestTemplate.exchange("/order/4/invoice", HttpMethod.GET, httpEntity, Invoice.class, headers)
        Invoice invoice = responseEntity.body
        then:
        assert responseEntity.statusCode.is2xxSuccessful()
        assert invoice.orderId == order.orderId
        assert invoice.items.size()!=0
    }

}
