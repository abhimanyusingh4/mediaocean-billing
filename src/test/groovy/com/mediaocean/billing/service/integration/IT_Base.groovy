package com.mediaocean.billing.service.integration;

import com.mediaocean.billing.BillingApplication
import com.mediaocean.billing.application.model.Customer
import com.mediaocean.billing.application.model.GstSlab
import com.mediaocean.billing.application.model.Order
import com.mediaocean.billing.application.model.Product
import com.mediaocean.billing.application.service.CustomerService
import com.mediaocean.billing.application.service.OrderService
import com.mediaocean.billing.application.service.ProductService
import com.mediaocean.billing.service.integration.stateStorage.State
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = BillingApplication.class)
@ActiveProfiles("test")
class IT_Base {

    @Autowired
    TestRestTemplate testRestTemplate

    @Autowired
    ProductService productService

    @Autowired
    OrderService orderService

    @Autowired
    CustomerService customerService


    @Test
    void "Test successful initialization"() {
        assert testRestTemplate != null
    }


    @Before
    void setup(){
        setupTestDB()
    }

    void setupTestDB() {

        if (State.state)
            return

        State.state = true
        def productList = [
                new Product(name: "Test Product 1", price: 123.0, slab: GstSlab.SLAB_A, brandName: "Test Brand 1"),
                new Product(name: "Test Product 2", price: 234.0, slab: GstSlab.SLAB_B, brandName: "Test Brand 2")
        ]

        productList = productService.addProducts(productList)

        def customer = customerService.addCustomer(new Customer(name: "Test User 1", email: "testEmail1@gmail.com"))

        def order = orderService.createOrder(customer.customerId)

        orderService.addProductById(order.orderId, productList.get(0).productId)
        orderService.addProductById(order.orderId, productList.get(1).productId)
        orderService.addProductById(order.orderId, productList.get(0).productId)
    }
}
