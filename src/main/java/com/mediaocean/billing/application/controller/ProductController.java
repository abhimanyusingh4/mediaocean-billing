package com.mediaocean.billing.application.controller;

import com.mediaocean.billing.application.exception.EntityValidationFailedException;
import com.mediaocean.billing.application.exception.ProductNotFoundException;
import com.mediaocean.billing.application.model.Product;
import com.mediaocean.billing.application.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/product")
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/{productId}")
    public @ResponseBody Product getProductById(@PathVariable("productId") Integer productId) throws ProductNotFoundException {
        return productService.getProductById(productId);
    }

    @PostMapping
    public @ResponseBody List<Product> addProducts(@RequestBody List<Product> products) throws EntityValidationFailedException {
        return productService.addProducts(products);
    }

}
