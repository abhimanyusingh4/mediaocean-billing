package com.mediaocean.billing.application.controller;

import com.mediaocean.billing.application.exception.CustomerNotFoundException;
import com.mediaocean.billing.application.exception.EntityValidationFailedException;
import com.mediaocean.billing.application.model.Customer;
import com.mediaocean.billing.application.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    private final CustomerService customerService;


    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/{email}")
    public @ResponseBody Customer getCustomerByEmail(@PathVariable("email") String customerId) throws CustomerNotFoundException {
        return customerService.getCustomerById(customerId);
    }

    @PutMapping
    public @ResponseBody Customer onboardCustomer(@RequestBody Customer customer) throws EntityValidationFailedException {
        return customerService.addCustomer(customer);
    }
}
