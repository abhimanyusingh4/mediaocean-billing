package com.mediaocean.billing.application.controller;

import com.mediaocean.billing.application.exception.CustomerNotFoundException;
import com.mediaocean.billing.application.exception.OrderNotFoundException;
import com.mediaocean.billing.application.exception.ProductNotFoundException;
import com.mediaocean.billing.application.model.Invoice;
import com.mediaocean.billing.application.model.Order;
import com.mediaocean.billing.application.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/order")
public class OrderController {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping("/{orderId}")
    public @ResponseBody Order getOrderDetailsById(@PathVariable("orderId") Integer orderId) throws OrderNotFoundException {
        return orderService.getOrderById(orderId);
    }

    @PutMapping("/{orderId}/product/{productId}")
    public @ResponseBody Order addProductById(@PathVariable("orderId") Integer orderId, @PathVariable("productId") Integer productId) throws OrderNotFoundException, ProductNotFoundException {
        return orderService.addProductById(orderId, productId);
    }

    @GetMapping("/{orderId}/invoice")
    public @ResponseBody Invoice getInvoice(@PathVariable("orderId") Integer orderId) throws OrderNotFoundException{
        return orderService.generateInvoice(orderId);
    }

    @PostMapping("/{customerId}/new")
    public @ResponseBody Order addOrder(@PathVariable(value = "customerId", required = false) Integer customerId) throws CustomerNotFoundException {
        return orderService.createOrder(customerId);
    }
}
