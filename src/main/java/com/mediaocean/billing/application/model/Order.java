package com.mediaocean.billing.application.model;



import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.annotation.Persistent;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Persistent
@Entity(name = "Orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ORDER_ID", nullable = false)
    private Integer orderId;

    @Column(name = "CUSTOMER_ID", insertable = false, updatable = false)
    private Integer customerId;


    @JsonInclude(JsonInclude.Include.NON_NULL)
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="ORDERS_PRODUCTS",
            joinColumns=
            @JoinColumn(name="ORDER_ID", referencedColumnName="ORDER_ID"),
            inverseJoinColumns=
            @JoinColumn(name="PRODUCT_ID", referencedColumnName="PRODUCT_ID")
    )
    private List<Product> products;

    @JsonIgnore
    @ManyToOne(optional=false)
    @JoinColumn(name="CUSTOMER_ID",referencedColumnName="CUSTOMER_ID")
    private Customer customer;

    private Double orderTotal=0d;

    private Double taxTotal=0d;

    private Double netInvoice=0d;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Double getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(Double orderTotal) {
        this.orderTotal = calculateOrderTotal();
    }

    public Double calculateOrderTotal(){
        return 0d;
    }

    public Double getTaxTotal() {
        return taxTotal;
    }

    public void setTaxTotal(Double taxTotal) {
        this.taxTotal = taxTotal;
    }

    public Double getNetInvoice() {
        return netInvoice;
    }

    public void setNetInvoice(Double netInvoice) {
        this.netInvoice = netInvoice;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(getOrderId(), order.getOrderId()) &&
                Objects.equals(getProducts(), order.getProducts()) &&
                Objects.equals(getOrderTotal(), order.getOrderTotal()) &&
                Objects.equals(getTaxTotal(), order.getTaxTotal()) &&
                Objects.equals(getNetInvoice(), order.getNetInvoice());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOrderId(), getProducts(), getOrderTotal(), getTaxTotal(), getNetInvoice());
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId='" + orderId + '\'' +
                ", products={" + (products!=null?products.stream().toString():"") + "}" +
                ", orderTotal=" + orderTotal +
                ", taxTotal=" + taxTotal +
                ", netInvoice=" + netInvoice +
                '}';
    }
}
