package com.mediaocean.billing.application.model;


import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity(name = "Products")
@Table(uniqueConstraints = @UniqueConstraint(name = "price_name", columnNames = {"name", "price"}))
public class Product {

    @Id
    @Column(name = "PRODUCT_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer productId;


    private String name;

    private GstSlab slab;

    private String brandName;

    private Double price = 0d;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GstSlab getSlab() {
        return slab;
    }

    public void setSlab(GstSlab slab) {
        this.slab = slab;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(getProductId(), product.getProductId()) &&
                Objects.equals(getName(), product.getName()) &&
                getSlab() == product.getSlab() &&
                Objects.equals(getBrandName(), product.getBrandName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProductId(), getName(), getSlab(), getBrandName());
    }

    @Override
    public String toString() {
        return "ProductRepo{" +
                "productId='" + productId + '\'' +
                ", name='" + name + '\'' +
                ", slab=" + slab + "Tax Rate: " + slab.getTaxRate() +
                ", brandName='" + brandName + '\'' +
                '}';
    }
}



