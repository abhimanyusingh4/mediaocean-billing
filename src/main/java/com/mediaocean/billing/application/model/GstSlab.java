package com.mediaocean.billing.application.model;

public enum GstSlab {
    SLAB_A(0), SLAB_B(5), SLAB_C(12), SLAB_D(18), SLAB_E(28);

    private Integer taxRate;

    GstSlab(Integer taxRate){
        this.taxRate = taxRate;
    }

    public int getTaxRate() {
        return taxRate;
    }

    @Override
    public String toString() {
        return taxRate.toString();
    }
}
