package com.mediaocean.billing.application.model;


import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.annotation.Persistent;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Persistent
@Entity(name = "CUSTOMER")
@Table(uniqueConstraints = @UniqueConstraint(name = "email", columnNames = {"email"}))

public class Customer {

    @Id
    @Column(name = "CUSTOMER_ID", nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer customerId;

    @Column(unique = true)
    private String email;

    private String name;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @OneToMany(mappedBy = "customer", targetEntity = Order.class,
            fetch = FetchType.EAGER)
    private List<Order> orders;

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return Objects.equals(getCustomerId(), customer.getCustomerId()) &&
                Objects.equals(getEmail(), customer.getEmail()) &&
                Objects.equals(getName(), customer.getName()) &&
                Objects.equals(getOrders(), customer.getOrders());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCustomerId(), getEmail(), getName(), getOrders());
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerId='" + customerId + '\'' +
                ", email='" + email + '\'' +
                ", name='" + name + '\'' +
                (orders != null ? (
                        ", orders=" + orders.stream().toString()) : "") +
                '}';
    }
}
