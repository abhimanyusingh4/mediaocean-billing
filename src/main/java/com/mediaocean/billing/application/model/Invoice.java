package com.mediaocean.billing.application.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Invoice {

    private Integer orderId;

    private List<Item> items;

    private Double netInvoice = 0d;

    private Double netTax = 0d;

    public Invoice(Order order) {
        this.orderId = order.getOrderId();
        items = Item.itemListBuilder(order.getProducts());
        for (Item item : items) {
            netInvoice += item.netPrice;
            netTax += item.netTax;
        }
    }

    public Invoice() {
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public Double getNetInvoice() {
        return netInvoice;
    }

    public void setNetInvoice(Double netInvoice) {
        this.netInvoice = netInvoice;
    }

    public Double getNetTax() {
        return netTax;
    }

    public void setNetTax(Double netTax) {
        this.netTax = netTax;
    }

    public static class Item {
        private Product product;
        private Double netPrice;
        private Double taxRate;
        private Double netTax;
        private Integer units;

        private Item(Product product, Integer units) {
            this.product = product;
            this.units = units;
            taxRate = product.getSlab().getTaxRate() / 100d;
            netTax = product.getPrice() * taxRate * units;
            netPrice = product.getPrice() + netTax * units;
        }

        public Item(){}

        static List<Item> itemListBuilder(List<Product> products) {
            Map<Product, Integer> productIntegerMap = createCountMap(products);
            List<Item> items = new LinkedList<>();
            productIntegerMap.keySet().forEach(product -> items.add(new Item(product, productIntegerMap.get(product))));
            return items;
        }

        private static Map<Product, Integer> createCountMap(List<Product> products) {
            Map<Product, Integer> productWiseCountMap = new HashMap<>();
            products.forEach(product -> {
                if (productWiseCountMap.containsKey(product)) {
                    int val = productWiseCountMap.get(product);
                    productWiseCountMap.put(product, ++val);
                } else {
                    productWiseCountMap.put(product, 1);
                }
            });

            return productWiseCountMap;
        }

        public Product getProduct() {
            return product;
        }

        public void setProduct(Product product) {
            this.product = product;
        }

        public Double getNetPrice() {
            return netPrice;
        }

        public void setNetPrice(Double netPrice) {
            this.netPrice = netPrice;
        }

        public Double getTaxRate() {
            return taxRate;
        }

        public void setTaxRate(Double taxRate) {
            this.taxRate = taxRate;
        }

        public Double getNetTax() {
            return netTax;
        }

        public void setNetTax(Double netTax) {
            this.netTax = netTax;
        }

        public Integer getUnits() {
            return units;
        }

        public void setUnits(Integer units) {
            this.units = units;
        }
    }
}
