package com.mediaocean.billing.application.repository;


import com.mediaocean.billing.application.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepo  extends JpaRepository<Order, Integer> {

}
