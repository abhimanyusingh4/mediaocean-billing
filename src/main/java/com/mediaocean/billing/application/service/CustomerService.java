package com.mediaocean.billing.application.service;

import com.mediaocean.billing.application.exception.CustomerNotFoundException;
import com.mediaocean.billing.application.exception.EntityValidationFailedException;
import com.mediaocean.billing.application.model.Customer;
import com.mediaocean.billing.application.repository.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {

    private final CustomerRepo customerRepo;

    @Autowired
    public CustomerService(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;
    }

    public Customer addCustomer(Customer customer) throws EntityValidationFailedException{
        try {
            return customerRepo.save(customer);
        }catch (Exception ex){
            throw new EntityValidationFailedException();
        }
    }

    public Customer getCustomerById(String email) throws CustomerNotFoundException {
        return customerRepo.findCustomerByEmail(email).orElseThrow(CustomerNotFoundException::new);
    }
}
