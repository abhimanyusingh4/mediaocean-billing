package com.mediaocean.billing.application.service;

import com.mediaocean.billing.application.exception.EntityValidationFailedException;
import com.mediaocean.billing.application.exception.ProductNotFoundException;
import com.mediaocean.billing.application.model.Product;
import com.mediaocean.billing.application.repository.ProductRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private static Logger LOGGER = LoggerFactory.getLogger(ProductService.class);

    private final ProductRepo productRepo;

    @Autowired
    public ProductService(ProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    public Product getProductById(Integer id) throws ProductNotFoundException {
        return productRepo.findById(id).orElseThrow(ProductNotFoundException::new);
    }

    public List<Product> addProducts(List<Product> products)throws EntityValidationFailedException{
        try{
            List<Product> validProducts = products.stream()
                    .filter(product -> Objects.nonNull(product.getPrice()) && product.getPrice()>0)
                    .collect(Collectors.toList());
            if (validProducts.size()<1){
                throw new EntityValidationFailedException();
            }
            return productRepo.saveAll(validProducts);
        }catch (Exception ex){
            LOGGER.error("Product Addition Failed due to: "+ ex.getCause().getMessage());
            throw new EntityValidationFailedException();
        }
    }


}
