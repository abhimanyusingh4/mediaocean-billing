package com.mediaocean.billing.application.service;

import com.mediaocean.billing.application.exception.CustomerNotFoundException;
import com.mediaocean.billing.application.exception.OrderNotFoundException;
import com.mediaocean.billing.application.exception.ProductNotFoundException;
import com.mediaocean.billing.application.model.Invoice;
import com.mediaocean.billing.application.model.Order;
import com.mediaocean.billing.application.model.Product;
import com.mediaocean.billing.application.repository.CustomerRepo;
import com.mediaocean.billing.application.repository.OrderRepo;
import com.mediaocean.billing.application.repository.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    private final OrderRepo orderRepo;

    private final CustomerRepo customerRepo;

    private final ProductRepo productRepo;

    @Autowired
    public OrderService(OrderRepo orderRepo, ProductRepo productRepo, CustomerRepo customerRepo) {
        this.orderRepo = orderRepo;
        this.productRepo = productRepo;
        this.customerRepo = customerRepo;
    }

    public Order addProductById(Integer orderId, Integer productId) throws OrderNotFoundException, ProductNotFoundException {
        Order order = orderRepo.findById(orderId).orElseThrow(OrderNotFoundException::new);
        Product product = productRepo.findById(productId).orElseThrow(ProductNotFoundException::new);
        order.getProducts().add(product);
        return orderRepo.save(order);
    }

    public Order getOrderById(Integer orderId) throws OrderNotFoundException {
        return orderRepo.findById(orderId).orElseThrow(OrderNotFoundException::new);
    }

    public Order createOrder(Integer customerId) throws CustomerNotFoundException {
        Order order = new Order();
        order.setCustomer(customerRepo.findById(customerId).orElseThrow(CustomerNotFoundException::new));

        return orderRepo.save(order);
    }

    public Invoice generateInvoice(Integer orderId) throws OrderNotFoundException{
        return new Invoice(orderRepo.findById(orderId).orElseThrow(OrderNotFoundException::new));
    }
}
