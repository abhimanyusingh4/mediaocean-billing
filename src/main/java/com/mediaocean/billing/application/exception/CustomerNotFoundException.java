package com.mediaocean.billing.application.exception;

public class CustomerNotFoundException extends Throwable {
    public CustomerNotFoundException(){
        super("Customer not found");
    }
}
