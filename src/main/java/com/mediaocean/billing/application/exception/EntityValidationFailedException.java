package com.mediaocean.billing.application.exception;

public class EntityValidationFailedException extends Throwable {
    public EntityValidationFailedException(){
        super("The request entity is invalid or doesn't meet the criteria");
    }
}
