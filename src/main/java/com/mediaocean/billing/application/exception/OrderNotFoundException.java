package com.mediaocean.billing.application.exception;

public class OrderNotFoundException extends Throwable {
    public OrderNotFoundException(){
        super("Order not found");
    }
}
