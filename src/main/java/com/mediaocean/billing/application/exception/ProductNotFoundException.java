package com.mediaocean.billing.application.exception;

public class ProductNotFoundException extends Throwable {
    public ProductNotFoundException(){
        super("Product not found");
    }
}
